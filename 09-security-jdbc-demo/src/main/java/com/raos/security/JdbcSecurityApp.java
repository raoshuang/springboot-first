package com.raos.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Security 第四个案例
 *  结合jdbc的运用
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
@SpringBootApplication
public class JdbcSecurityApp {

    public static void main(String[] args) {
        SpringApplication.run(JdbcSecurityApp.class, args);
    }

}
