package com.raos.security.dao;

import com.raos.security.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 用户持久化接口
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
public interface UserInfoDao extends JpaRepository<UserInfo, Long> {

    /**
     * 根据 username 查询用户数据
     * @param username 用户名
     * @return 用户信息
     */
    UserInfo findByUsername(String username);

}
