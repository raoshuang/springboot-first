package com.raos.security.init;

import com.raos.security.dao.UserInfoDao;
import com.raos.security.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 表数据初始类
 *  初次开启注解 @Component, @PostConstruct，之后注释掉这两注解，避免多次创建数据。
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
//@Component
public class JdbcInit {

    @Autowired
    private UserInfoDao dao;

//    @PostConstruct
    public void init() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("zhangsan");
        userInfo.setPassword(encoder.encode("123456"));
        userInfo.setRole("normal");
        dao.save(userInfo);

        userInfo = new UserInfo();
        userInfo.setUsername("admin");
        userInfo.setPassword(encoder.encode("admin"));
        userInfo.setRole("admin");
        dao.save(userInfo);
    }

}
