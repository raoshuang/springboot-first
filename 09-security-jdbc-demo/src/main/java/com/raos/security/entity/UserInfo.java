package com.raos.security.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 用户实体类
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
@Data
@Entity
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //用户名称
    private String username;
    //密码
    private String password;
    //角色
    private String role;

}
