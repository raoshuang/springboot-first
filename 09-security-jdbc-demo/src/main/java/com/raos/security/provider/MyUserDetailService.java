package com.raos.security.provider;

import com.raos.security.dao.UserInfoDao;
import com.raos.security.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户详细信息服务类
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
@Component("myUserDetailService")
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserInfoDao dao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = null;
        UserInfo userinfo = null;
        if (username != null) {
            userinfo = dao.findByUsername(username);
            if (userinfo != null) {
                List<GrantedAuthority> list = new ArrayList<>();
                // 角色必须以ROLE_开头
                GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + userinfo.getRole());
                list.add(authority);
                // 创建User对象
                user = new User(userinfo.getUsername(), userinfo.getPassword(), list);
            }
        }
        return user;
    }

}
