package com.raos.security.service;

import com.raos.security.entity.UserInfo;

/**
 * 用户业务层接口
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
public interface UserInfoService {

    /**
     * 根据 username 查询用户数据
     * @param username 用户名
     * @return 用户信息
     */
    UserInfo findUserInfo(String username);
}
