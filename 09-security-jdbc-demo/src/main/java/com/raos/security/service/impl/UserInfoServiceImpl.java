package com.raos.security.service.impl;

import com.raos.security.dao.UserInfoDao;
import com.raos.security.entity.UserInfo;
import com.raos.security.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户业务层实现类
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:25
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao dao;

    @Override
    public UserInfo findUserInfo(String username) {
        return dao.findByUsername(username);
    }

}
