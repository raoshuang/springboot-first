package com.raos.security2role.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 第三个 Security 测试控制器
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:05
 */
@RestController
public class ThirdController {

    //指定 normal 和admin 角色都可以访问的方法
    @RequestMapping("/helloUser")
    @PreAuthorize(value = "hasAnyRole('admin', 'normal')")
    public String helloCommonUser() {
        return "Hello 拥有normal, admin角色的用户";
    }

    //指定admin角色的访问方法
    @RequestMapping("/helloAdmin")
    @PreAuthorize("hasAnyRole('admin')")
    public String helloAdmin() {
        return "Hello admin角色的用户可以访问";
    }

}
