package com.raos.security2role;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Security 入门启动类
 * 使用内存中的用户信息并且设置用户的权限案例
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 23:05
 */
@SpringBootApplication
public class SecurityDemoApp3 {

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApp3.class, args);
    }

}
