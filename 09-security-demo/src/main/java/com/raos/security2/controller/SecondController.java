package com.raos.security2.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 第二个 Security 测试控制器
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 22:48
 */
@RestController
public class SecondController {

    @RequestMapping("/hello")
    public String sayHello() {
        return "使用内存中的用户信息";
    }

}
