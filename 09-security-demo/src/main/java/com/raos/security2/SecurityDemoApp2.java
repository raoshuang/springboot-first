package com.raos.security2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Security 入门启动类
 * 使用内存中的用户信息案例
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 22:48
 */
@SpringBootApplication
public class SecurityDemoApp2 {

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApp2.class, args);
    }

}
