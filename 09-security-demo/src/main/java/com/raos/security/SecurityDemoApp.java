package com.raos.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Security 入门启动类
 * 使用配置文件或临时密码的入门案例
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 22:28
 */
//@SpringBootApplication(exclude = {SecurityAutoConfiguration.class}) //排除 Secuirty 的配置，让他不启用
@SpringBootApplication
public class SecurityDemoApp {

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApp.class, args);
    }

}
