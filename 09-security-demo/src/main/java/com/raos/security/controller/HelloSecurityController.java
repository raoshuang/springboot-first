package com.raos.security.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Security 入门控制器案例
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 22:28
 */
@RestController
@RequestMapping("/hello")
public class HelloSecurityController {

    @RequestMapping("/world")
    public String sayHello() {
        return "Hello Spring Secuirty 安全管理框架";
    }

}
