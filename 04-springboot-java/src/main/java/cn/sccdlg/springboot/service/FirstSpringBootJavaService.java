package cn.sccdlg.springboot.service;

/**
 * 第一个springboot java项目接口
 */
public interface FirstSpringBootJavaService {
    String sayHi(String name);
}
