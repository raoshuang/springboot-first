package cn.sccdlg.springboot.service.impl;

import cn.sccdlg.springboot.service.FirstSpringBootJavaService;
import org.springframework.stereotype.Service;

/**
 * 第一个springboot java项目接口实现类
 */
@Service
public class FirstSpringBootJavaServiceImpl implements FirstSpringBootJavaService {

    @Override
    public String sayHi(String name) {
        return "Hi," + name;
    }
}
