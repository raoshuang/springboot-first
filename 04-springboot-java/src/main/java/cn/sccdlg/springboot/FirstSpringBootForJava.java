package cn.sccdlg.springboot;

import cn.sccdlg.springboot.service.FirstSpringBootJavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/**
 * Springboot 纯java项目实现方式一
 */
public class FirstSpringBootForJava implements CommandLineRunner {

    @Autowired
    private FirstSpringBootJavaService firstSpringBootJavaService;

    /**
     * SpringBoot的启动入口，启动spring容器
     * @param args
     */
    public static void main(String[] args) {
        /*SpringApplication.run(FirstSpringBootForJava.class,args);*/
        //关闭Springboot启动时的logo
        SpringApplication application = new SpringApplication(FirstSpringBootForJava.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }

    /**
     * 相当于纯Java项目的main方法入口
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        String s = firstSpringBootJavaService.sayHi("Springboot for java ");
        System.out.println("*********");
        System.out.println(s);
    }
}
/**
 * Springboot 纯java项目实现方式二
 */
/*public class FirstSpringBootForJava {
    public static void main(String[] args) {
        //返回spring容器对象
        ConfigurableApplicationContext context = SpringApplication.run(FirstSpringBootForJava.class, args);
        FirstSpringBootJavaService firstSpringBootJavaService = (FirstSpringBootJavaService)context.getBean("firstSpringBootJavaServiceImpl");
        System.out.println("**************");
        String hi = firstSpringBootJavaService.sayHi("Springboot main for java");
        System.out.println(hi);
    }
}*/
