package cn.sccdlg.springboot.servcie;

import cn.sccdlg.springboot.model.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * 分页查询.
     * @param paramMap 多个参数
     * @return
     */
    List<User> getUserByPage(Map<String, Object> paramMap);
     /**
      * 分页需要查询数据总数
      * @return
      */
    int getUserByTqtal();
    /**
     * 添加用户
     * @param user
     * @return
     */
    int addUser(User user);
    /**
     * 修改用户
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 通过id，删除用户
     * @param id
     * @return
     */
    int deleteUser(Integer id);

    /**
     * 通过id查询用户
     * @param id
     * @return
     */
    User getUserById(Integer id);
}