package com.raos.log;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 集成Log4j日志测试
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/07 20:48
 */
@SpringBootApplication
public class LogAppTest {

    private static final Logger LOG = Logger.getLogger(LogAppTest.class);

    public static void main(String[] args) {
        SpringApplication.run(LogAppTest.class, args);
        LOG.debug("我是debug级别的日志消息");
        LOG.info("我是info级别的日志消息");
        LOG.warn("我是warn级别的日志消息");
        LOG.error("我是error级别的日志消息");
    }

}
