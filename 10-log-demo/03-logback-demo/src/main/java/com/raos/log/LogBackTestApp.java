package com.raos.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 集成logback日志测试
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/09/07 20:48
 */
@SpringBootApplication
public class LogBackTestApp {

    private static final Logger LOG = LoggerFactory.getLogger(LogBackTestApp.class);

    public static void main(String[] args) {
        SpringApplication.run(LogBackTestApp.class, args);
        LOG.debug("我是debug级别的日志消息");
        LOG.info("我是info级别的日志消息");
        LOG.warn("我是warn级别的日志消息");
        LOG.error("我是error级别的日志消息");
    }

    /**
     * springboot logback 日志默认配置文件所属位置在对应的spring-boot jar中的 org.springframework.boot.logging.logback 目录
     * 主要由4个文件构成，base.xml，console-appender.xml，defaults.xml，file-appender.xml
     */
}
