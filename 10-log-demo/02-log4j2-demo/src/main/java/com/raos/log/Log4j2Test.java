package com.raos.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 集成Log4j2日志测试
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/07 20:58
 */
@SpringBootApplication
public class Log4j2Test {

    private static final Logger LOG = LoggerFactory.getLogger(Log4j2Test.class);

    public static void main(String[] args) {
        SpringApplication.run(Log4j2Test.class, args);
        LOG.debug("我是debug级别的日志消息");
        LOG.info("我是info级别的日志消息");
        LOG.warn("我是warn级别的日志消息");
        LOG.error("我是error级别的日志消息");
    }


    /**
     * springboot log4j2 日志默认配置文件所属位置在对应的spring-boot jar中的 org.springframework.boot.logging.log4j2 目录
     * 主要由2个文件构成，log4j2.xml，log4j2-file.xml
     */

}
