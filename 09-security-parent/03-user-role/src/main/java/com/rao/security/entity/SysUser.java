package com.rao.security.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 用户实体类
 *  实现 UserDetails 接口
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 21:23
 */
@Data
public class SysUser implements UserDetails {

    // 主键
    private Integer id;
    // 用户名
    private String username;
    // 密码
    private String password;
    // 真实姓名
    private String realname;

    // 是否过期(0-过期,1-存活)
    private boolean isExpired;
    // 是否锁定
    private boolean isLocked;
    // 是否有凭证
    private boolean isCredentials;
    // 是否启用
    private boolean isEnabled;

    // 创建时间
    private Date createTime;
    // 登录时间
    private Date loginTime;

    // 权限列表
    private List<GrantedAuthority> authorities;

    /**
     * 无参构造
     */
    public SysUser() { }

    /**
     * 有参构造
     */
    public SysUser(String username, String password, String realname, boolean isExpired,
                   boolean isLocked, boolean isCredentials, boolean isEnabled, Date createTime, Date loginTime,
                   List<GrantedAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.realname = realname;
        this.isExpired = isExpired;
        this.isLocked = isLocked;
        this.isCredentials = isCredentials;
        this.isEnabled = isEnabled;
        this.createTime = createTime;
        this.loginTime = loginTime;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isCredentials;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    @Override
    public String toString() {
        return "SysUser{" + "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", realname='" + realname + '\'' +
                ", isExpired=" + isExpired +
                ", isLocked=" + isLocked +
                ", isCredentials=" + isCredentials +
                ", isEnabled=" + isEnabled +
                ", createTime=" + createTime +
                ", loginTime=" + loginTime +
                ", authorities=" + authorities + '}';
    }

}
