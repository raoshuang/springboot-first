package com.rao.security.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 消息响应对象
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:22
 */
@AllArgsConstructor
@Data
public class Result {

    // 状态码，0-成功，1-失败
    private int code;
    // 错误码
    private int error;
    // 消息文本
    private String msg;

    public Result() { }

}
