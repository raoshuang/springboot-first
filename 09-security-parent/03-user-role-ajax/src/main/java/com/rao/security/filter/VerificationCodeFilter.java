package com.rao.security.filter;

import com.rao.security.common.MyFailureHandler;
import com.rao.security.common.Result;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 验证码过滤器
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:42
 */
public class VerificationCodeFilter extends OncePerRequestFilter {

    private MyFailureHandler failureHandler = new MyFailureHandler();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        System.out.println("VerificationCodeFilter doFilterInternal");
        // 只有是login操作，才需要这个过滤器参与验证码的使用
        String uri = request.getRequestURI();
        if (!"/login".equals(uri)) {
            //过滤器正常执行，不参与验证码操作
            filterChain.doFilter(request, response);
        } else {
            // 登录操作，需要验证code
            try {
                // 验证：code是否正确
                verifcatioinCode(request);
                // 如果验证通过，过滤器正常执行
                filterChain.doFilter(request, response);

            } catch (VerificationException e) {
                Result result = new Result(1, 1002, "验证码错误!!!");
                failureHandler.setResult(result);
                failureHandler.onAuthenticationFailure(request, response, e);
            }
        }
    }

    /**
     * 验证验证码（验证码不区分大小写）
     * @param request
     */
    private void verifcatioinCode(HttpServletRequest request) {
        HttpSession session = request.getSession();
        // 获取请求中的code
        String reqCode = request.getParameter("code").toLowerCase();

        // 获取session中的code
        String sessionCode = "";
        Object attr = session.getAttribute("code");
        if (attr != null) {
            sessionCode = ((String) attr).toLowerCase();
        }
        System.out.println("VerificationCodeFilter doFilterInternal reqCode: "
                + reqCode + " | sessionCode: " + sessionCode);

        //处理逻辑
        if (!StringUtils.isEmpty(sessionCode)) {
            //在session中的code， 用户看到这个code了。
            //如果能到这段代码，说明用户已经发起了登录请求的。
            //session中的现在的这个code就应该无用
            session.removeAttribute("code");
        }

        // 判断code是否正确。
        if (StringUtils.isEmpty(reqCode) || StringUtils.isEmpty(sessionCode) ||
                !reqCode.equals(sessionCode)) {
            // 失败
            throw new VerificationException();
        }
    }

}
