package com.rao.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 首页控制器
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:21
 */
@Controller
public class IndexController {

    @GetMapping("/index")
    public String toIndexHtml() {
        return "forward:/index.html";
    }

}
