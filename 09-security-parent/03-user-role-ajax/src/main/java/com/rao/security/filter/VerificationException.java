package com.rao.security.filter;

import org.springframework.security.core.AuthenticationException;

/**
 * 验证码错误异常类
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:42
 */
public class VerificationException extends AuthenticationException {

    public VerificationException(String msg, Throwable t) {
        super(msg, t);
    }

    public VerificationException(String msg) {
        super(msg);
    }

    public VerificationException() {
        super("验证码错误, 请重新输入");
    }

}
