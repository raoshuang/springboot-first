package com.rao.security.entity;

import lombok.Data;

/**
 * 角色实体
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:21
 */
@Data
public class SysRole {

    // 主键
    private Integer id;
    // 角色名
    private String roleName;
    // 角色说明
    private String roleMemo;

    @Override
    public String toString() {
        return "SysRole{" + "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", roleMemo='" + roleMemo + '\'' + '}';
    }

}
