package com.rao.security.mapper;

import com.rao.security.entity.SysRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户操作持久层接口
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:21
 */
@Repository
public interface SysRoleMapper {

    /**
     * 根据用户id获取角色列表
     * @param userId 用户id
     * @return
     */
    List<SysRole> selectRoleByUser(Integer userId);

}
