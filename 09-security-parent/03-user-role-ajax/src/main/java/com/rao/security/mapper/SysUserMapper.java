package com.rao.security.mapper;

import com.rao.security.entity.SysUser;
import org.springframework.stereotype.Repository;

/**
 * 用户操作持久层接口
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 22:21
 */
@Repository
public interface SysUserMapper {

    /**
     * 添加用户
     * @param user 用户信息
     * @return
     */
    int insertSysUser(SysUser user);

    /**
     * 根据账号名称，获取用户信息
     * @param username 用户名
     * @return
     */
    SysUser selectSysUser(String username);

}
