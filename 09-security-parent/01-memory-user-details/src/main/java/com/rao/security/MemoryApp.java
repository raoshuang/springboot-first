package com.rao.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * security - 内存性用户信息权限限定
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 20:11
 */
@SpringBootApplication
public class MemoryApp {

    public static void main(String[] args) {
        SpringApplication.run(MemoryApp.class, args);
    }

}
