01-memory-user-details：使用 InMemoryUserDetailsManager 管理内存中用户信息

实现步骤：
1.新建maven项目2.加入依赖gav坐标
 1) spring-boot : 2.2.6
 2) spring-boot-security-starter
 3) spring-boot-web-starter

3.创建应用的配置类
 1）创建密码的处理类对象
 2）使用InMemoryUserDetailsManager创建用户

4.创建类继承WebSecurityConfigurerAdapter.
 自定义安全配置

5.测试


