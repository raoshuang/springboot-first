package com.rao.security.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 数据库用户信息控制器
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 20:23
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String sayHello() {
        return "JdbcUserDetailsManager 管理用户信息";
    }

    //指定 normal 和admin 角色都可以访问的方法
    @RequestMapping("/helloUser")
    @PreAuthorize(value = "hasAnyRole('ADMIN','USER','MANAGER')")
    public String helloCommonUser() {
        return "===Hello 拥有user,manager , admin角色的用户===";
    }

    //指定admin角色的访问方法
    @RequestMapping("/helloAdmin")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public String helloAdmin() {
        return "===Hello admin角色的用户==";
    }

}
