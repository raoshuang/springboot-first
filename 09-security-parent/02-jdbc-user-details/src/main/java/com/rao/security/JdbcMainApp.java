package com.rao.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * security - 数据库用户信息权限限定
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/06 20:23
 */
@SpringBootApplication
public class JdbcMainApp {

    public static void main(String[] args) {
        SpringApplication.run(JdbcMainApp.class, args);
    }

}
