package cn.sccdlg.springboot.mybatis;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 测试主类
 *
 * @author raos
 * @date 2024-11-24
 */
@SpringBootTest
public class SpringbootMybatisApplicationTest {

    @Test
    void contextLoads() {

    }

}
