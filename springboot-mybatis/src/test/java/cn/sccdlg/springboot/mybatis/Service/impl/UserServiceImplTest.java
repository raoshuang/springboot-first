package cn.sccdlg.springboot.mybatis.Service.impl;

import cn.sccdlg.springboot.mybatis.Service.UserService;
import cn.sccdlg.springboot.mybatis.mapper.UserMapper;
import cn.sccdlg.springboot.mybatis.model.User;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * mybatis 批量插入测试
 * 结论：
 * 1. for循环单条插入，效率低，但比 for循环 ExecutorType.BATCH单条插入要好（数值越大越明显）
 * 2. foreach批量插入，效率高，ExecutorType.BATCH 结合 foreach 批量插入，效率相比较高（数值超过10万级越明显）
 *
 * @author raos
 * @date 2024-11-24
 */
@SpringBootTest
class UserServiceImplTest {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(UserServiceImplTest.class);

    /**
     * 批量插入记录数（分别设置如下）
     * 1000条
     * 10000条
     * 100000条
     * 1000000条
     */
    private static final int COUNT_NUM = 100000;

    @Autowired
    private SqlSessionFactory sqlSessionFactory;
    @Autowired
    private UserService userService;

    /**
     * for循环单条
     * 1000条，耗时：1204ms
     * 10000条，耗时：7441ms
     * 100000条，耗时：41219ms
     */
    @Test
    void insertForUser() {
        Instant start= Instant.now();
        try {
            List<User> userList = new ArrayList<>();
            for (int i = 0; i < COUNT_NUM; i++) {
                User user = new User();
                user.setName("用户名A" + i);
                user.setPassword("密码A" + i);
                user.setSal("SalA" + i);
                user.setJob("JobA" + i);

                userList.add(user);

                // 判断数目
                if (userList.size() == 1000) {
                    userService.insertUsers(userList);
                    userList.clear();
                }
            }
            if (!CollectionUtils.isEmpty(userList)) {
                userService.insertUsers(userList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("**for循环单条写入总共用时："+ ChronoUnit.MILLIS.between(start,Instant.now()) +"毫秒**");
    }

    /**
     * foreach批量插入
     * 1000条，耗时：539ms
     * 10000条，耗时：1428ms
     * 100000条，耗时：9023ms
     * 1000000条，耗时：89874ms
     */
    @Test
    void insertForeachUser() {
        Instant start= Instant.now();
        try {
            List<User> userList = new ArrayList<>();
            for (int i = 0; i < COUNT_NUM; i++) {
                User user = new User();
                user.setName("用户名B" + i);
                user.setPassword("密码B" + i);
                user.setSal("SalB" + i);
                user.setJob("JobB" + i);

                userList.add(user);
                // 判断数目
                if (userList.size() == 1000) {
                    userService.insertUsers2(userList);
                    userList.clear();
                }
            }
            if (!CollectionUtils.isEmpty(userList)) {
                userService.insertUsers2(userList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("**foreach批量写入总共用时："+ ChronoUnit.MILLIS.between(start,Instant.now()) +"毫秒**");
    }

    /**
     * 基于 ExecutorType.BATCH 普通批量插入
     * 1000条，耗时：64867ms
     * 10000条，耗时：638155ms
     * 100000条，耗时：不用看了
     *
     * 数据库连接参数添加：rewriteBatchedStatements=true后的执行效率
     * 10000条，耗时：1203ms
     * 100000条，耗时：8269ms
     * 1000000条，耗时：78251ms
     */
    @Test
    void insertExecutorTypeBatchUser() {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Instant start= Instant.now();
        try {
            for (int i = 1; i <= COUNT_NUM; i++) {
                User user = new User();
                user.setName("用户名C" + i);
                user.setPassword("密码C" + i);
                user.setSal("SalC" + i);
                user.setJob("JobC" + i);

                userMapper.insert(user);

                // 判断数目
                if (i % 1000 == 0) {
                    logger.info("ExecutorTypeBatch批量写入记录：{}", i);
                    sqlSession.commit();
                    sqlSession.clearCache();
                }
            }
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        System.out.println("**ExecutorTypeBatch批量写入总共用时："+ ChronoUnit.MILLIS.between(start,Instant.now()) +"毫秒**");
    }

    /**
     * 基于 ExecutorType.BATCH + foreach 批量插入
     * 1000条，耗时：534ms
     * 10000条，耗时：1429ms
     * 100000条，耗时：9016ms
     * 1000000条，耗时：84618ms
     */
    @Test
    void insertForeachExecutorTypeBatchUser() {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Instant start= Instant.now();
        try {
            List<User> userList = new ArrayList<>();
            for (int i = 1; i <= COUNT_NUM; i++) {
                User user = new User();
                user.setName("用户名D" + i);
                user.setPassword("密码D" + i);
                user.setSal("SalD" + i);
                user.setJob("JobD" + i);
                userList.add(user);

                // 判断数目
                if (i % 1000 == 0) {
                    userMapper.insertBatch(userList);
                    userList.clear();
                    logger.info("ForeachExecutorTypeBatch批量写入记录：{}", i);
                    sqlSession.commit();
                    sqlSession.clearCache();
                }
            }
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        System.out.println("**ExecutorTypeBatch批量写入总共用时："+ ChronoUnit.MILLIS.between(start,Instant.now()) +"毫秒**");
    }

}