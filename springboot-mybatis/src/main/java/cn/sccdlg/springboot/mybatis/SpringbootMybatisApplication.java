package cn.sccdlg.springboot.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("cn.sccdlg.springboot.mybatis.mapper")
//@EnableTransactionManagement 表示开启事务支持
@EnableTransactionManagement
public class SpringbootMybatisApplication {

    public static void main(String[] args) {
        //启动springboot程序，启动spring容器，启动了内嵌的tomcat
        SpringApplication.run(SpringbootMybatisApplication.class, args);
        System.out.println("启动成功！");
    }

}
