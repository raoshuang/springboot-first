package cn.sccdlg.springboot.mybatis.Service;

import cn.sccdlg.springboot.mybatis.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUser();

    /**
     * 普通for循环批量插入
     */
    void insertUsers(List<User> users);

    /**
     * foreach批量插入
     */
    void insertUsers2(List<User> users);

    /**
     * 基于 ExecutorType.BATCH 批量插入（实际上执行单条插入反而不如普通的insert）
     * @param users
     */
    void insertUsers3(List<User> users);


}
