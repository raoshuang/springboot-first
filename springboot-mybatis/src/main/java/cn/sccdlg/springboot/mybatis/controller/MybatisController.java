package cn.sccdlg.springboot.mybatis.controller;

import cn.sccdlg.springboot.mybatis.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class MybatisController {
    @Autowired
    private UserService userService;
    @RequestMapping("/boot/getUser")
    public @ResponseBody Object getAllUser(){
        //设置线程，该线程调用底层查询所有学生的方法
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                userService.getAllUser();
            }
        };
        //多线程测试缓存穿透方案解决情况
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i=0;i<10000;i++) {
            executorService.submit(runnable);
        }
        return userService.getAllUser();
    }
}
