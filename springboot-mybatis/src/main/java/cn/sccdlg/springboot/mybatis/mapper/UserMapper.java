package cn.sccdlg.springboot.mybatis.mapper;

import cn.sccdlg.springboot.mybatis.model.User;

import java.util.List;

//@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    /**
     * 批量插入用户信息
     *
     * @param userList
     * @return
     */
    int insertBatch(List<User> userList);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> getAllUser();
}