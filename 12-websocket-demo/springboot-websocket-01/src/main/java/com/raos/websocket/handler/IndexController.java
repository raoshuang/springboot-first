package com.raos.websocket.handler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * IndexController
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 9:05
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        System.out.println("接收根路径请求");
        return "myWebsocket";
    }

}
