package com.raos.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 整合 websocket 案例（一）
 * 启动类
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 9:03
 */
@SpringBootApplication
public class WebsocketApp {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketApp.class, args);
    }

}
