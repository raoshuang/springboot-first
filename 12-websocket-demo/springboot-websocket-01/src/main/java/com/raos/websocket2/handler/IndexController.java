package com.raos.websocket2.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;

/**
 * IndexController
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 9:05
 */
@Controller
public class IndexController {

    @Autowired
    private WebSocketHandler myHandler;

    @GetMapping("/")
    public String index() {
        System.out.println("接收根路径请求");
        return "socket";
    }

    @ResponseBody
    @GetMapping("/sendMsg")
    public String sendMsg() {
        String msg = "hello webSocket, send msg to all...";
        ((MyHandler)myHandler).sendMessageToAllUsers(new TextMessage(msg));
        return msg;
    }

}
