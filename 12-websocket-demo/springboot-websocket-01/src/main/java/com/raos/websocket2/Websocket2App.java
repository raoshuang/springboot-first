package com.raos.websocket2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 整合 websocket 案例（二）
 *      【基于H5】进行点对点[一对一] 和 广播[一对多]实时推送
 * 启动类
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 9:32
 */
@SpringBootApplication
public class Websocket2App {

    public static void main(String[] args) {
        SpringApplication.run(Websocket2App.class, args);
    }

}
