package com.raos.websocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * WebSocketApp
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 10:22
 */
@SpringBootApplication
public class WebSocketApp {

    public static void main(String[] args) {
        SpringApplication.run(WebSocketApp.class, args);
    }

}
