package com.raos.websocket.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * WebSocketConfig
 * 开启 WebSocket 支持
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 10:23
 */
@Configuration
public class WebSocketConfig {

    /**
     * Bean 的作用: 自动注册使用了 @ServerEndpoint 注解声明的 Websocket endpoint（又称访问端点）
     * 对于部署至外部容器诸如 Tomcat，需要将此 Bean 创建注释，以免项目启动不成功
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

}
