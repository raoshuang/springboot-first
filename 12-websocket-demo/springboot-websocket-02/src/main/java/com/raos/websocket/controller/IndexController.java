package com.raos.websocket.controller;

import com.raos.websocket.server.WebSocketServer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * IndexController
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/16 10:41
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String index() {
        return this.page();
    }

    @GetMapping("/index")
    public String page() {
        System.out.println("请求成功");
        return "websocket";
    }

    @ResponseBody
    @RequestMapping("/push/{toUserId}")
    public ResponseEntity<String> pushToWeb(String message, @PathVariable String toUserId) throws IOException {
        WebSocketServer.sendInfo(message, toUserId);
        return ResponseEntity.ok("MSG SEND SUCCESS");
    }

    @ResponseBody
    @GetMapping("/pushAll")
    public ResponseEntity<String> pushAll(String message) throws IOException {
        WebSocketServer.sendAll(message);
        return ResponseEntity.ok("MSG SEND SUCCESS TO All");
    }

}
