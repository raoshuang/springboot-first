package cn.sccdlg.springboot.mapper;

import cn.sccdlg.springboot.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 分页查询
     * @param paramMap
     * @return
     */
    List<User> selectUserByPage(Map<String, Object>  paramMap);

    /**
     * 查询分页需要数据总数
     * @return
     */
    int selectUserByTqtal();
}