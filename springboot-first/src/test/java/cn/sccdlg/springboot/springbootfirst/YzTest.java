package cn.sccdlg.springboot.springbootfirst;

public class YzTest {
    private YzTest(Object o) {
        System.out.println("Object");
    }

    private YzTest(double[] dArray) {
        System.out.println("double array");
    }

    public static void main(String[] args) {
        new YzTest(null);//输出：double array
        //Java的重载解析过程是以两阶段运行的。
        // 第一阶段选取所有可获得并且可应用的方法或构造器。
        // 第二阶段在第一阶段选取的方法或构造器中选取最精确的一个。
        // 如果一个方法或构造器可以接受传递给另一个方法或构造器的任何参数，
        // 那么我们就说第一个方法比第二个方法缺乏精确性
        // jvm调用构造函数时优先进行精确性匹配，因此选择double[] 参数类型进行输出
        //new YzTest((Object) null);
    }

}
