package cn.sccdlg.springboot.springbootfirst.controller;

import cn.sccdlg.springboot.springbootfirst.model.Uesr;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //@RestController = @Controller + @ResponseBody
public class MvcController {

    @RequestMapping("/boot/getUser")
    public Object getUesr(){
        Uesr uesr = new Uesr();
        uesr.setId("100");
        uesr.setName("张三丰");
        return uesr;
    }
    /*
     * 支持get请求
     */
    @GetMapping("/boot/getUser1")//@RequestMapping + RequestMethod.GET
    public Object getUesr1(){
        Uesr uesr = new Uesr();
        uesr.setId("100");
        uesr.setName("张三丰_GetMapping");
        return uesr;
    }
    @PostMapping("/boot/getUser2")//@RequestMapping + RequestMethod.POST
    public Object getUesr2(){
        Uesr uesr = new Uesr();
        uesr.setId("100");
        uesr.setName("张三丰_PostMapping");
        return uesr;
    }
}
