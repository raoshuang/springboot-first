package cn.sccdlg.springboot.springbootfirst.config;

import cn.sccdlg.springboot.springbootfirst.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//拦截器的使用
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors (InterceptorRegistry registry) {
        //需要拦截的路径
        String[] addPathPatterns = {
                "/boot/**"
        };
        //不用拦截的路径
        String[] excludePathPatterns = {
                "/boot/hello",
                "/boot/index"
        };
        registry. addInterceptor(new LoginInterceptor())
        .addPathPatterns(addPathPatterns)
        .excludePathPatterns(excludePathPatterns);
    }

}
