package cn.sccdlg.springboot.springbootfirst.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 一个普通的servlet
 */
public class HeServlet extends HttpServlet {

    private static final long serialVersionUID = -4134217146900871026L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().print("<h1>his servlet hello word！</h1>");
        resp.getWriter().print("<h3>欢迎来到springboot教学</h3>");
        resp.getWriter().flush();
        resp.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }

}
