package cn.sccdlg.springboot.springbootfirst.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JspController {
    @RequestMapping("/boot/index")
    public String index(Model model){
        model.addAttribute("name","hello,springboot 集成jsp");
        return "index";
    }
}
