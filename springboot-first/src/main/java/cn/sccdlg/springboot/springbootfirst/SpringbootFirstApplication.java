package cn.sccdlg.springboot.springbootfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan({"cn.sccdlg.springboot.springbootfirst.servlet",
        "cn.sccdlg.springboot.springbootfirst.filter"})//开启servlet,filter自动配置包扫描
public class SpringbootFirstApplication {

    public static void main(String[] args) {
        //启动springboot程序，启动spring容器，启动了内嵌的tomcat
        SpringApplication.run(SpringbootFirstApplication.class, args);
    }

}
