package cn.sccdlg.springboot.springbootfirst.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * 一个普通的filter
 */
public class HeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("您已进入his filter过滤器,您的请求正常,请继续遵规则..");
        chain.doFilter(request, response);
    }
    @Override
    public void destroy() {

    }
}
