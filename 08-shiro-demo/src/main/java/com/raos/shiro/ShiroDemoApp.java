package com.raos.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Shiro 入门案例
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 20:28
 */
@SpringBootApplication
public class ShiroDemoApp {

    public static void main(String[] args) {
        SpringApplication.run(ShiroDemoApp.class, args);
    }

}
