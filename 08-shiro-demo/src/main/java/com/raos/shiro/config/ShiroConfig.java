package com.raos.shiro.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.raos.shiro.realm.UserRealm;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Shiro 入门配置
 *
 * @author raos
 * @emil 991207823@qq.com
 * @date 2021/09/05 20:28
 */
//@Configuration 标记当前类是一个Spring的配置类，用于模拟Spring的配置文件, 在这里我们将要配置Shiro
@Configuration
public class ShiroConfig {

    /**
     * 配置Shiro的安全管理器
     * @param myRealm
     * @return
     */
    @Bean
    public SecurityManager securityManager(Realm myRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //设置一个Realm，这个Realm是最终用于完成我们的认证号和授权操作的具体对象
        securityManager.setRealm(myRealm);
        return securityManager;
    }

    /**
     * 配置一个自定义的Realm的bean，最终将使用这个bean返回的对象来完成我们的认证和授权
     */
    @Bean
    public Realm myRealm() {
        UserRealm myRealm = new UserRealm();
        return myRealm;
    }

    /**
     * 配置一个Shiro的过滤器bean，这个bean将配置Shiro相关的一个规则的拦截
     *  例如什么样的请求可以访问什么样的请求不可以访问等等
     * @param securityManager
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        // 创建过滤器配置Bean
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // 配置用户登录请求如果需要进行登录时shiro就会转到这个请求进入登录页面
        shiroFilterFactoryBean.setLoginUrl("/");
        // 配置登录成功后转向的地址
        shiroFilterFactoryBean.setSuccessUrl("/success");
        // 配置没有权限时转向的地址
        shiroFilterFactoryBean.setUnauthorizedUrl("/noPermission");
        /**
         * 配置拦截规则
         */
        Map<String, String> filterChainMap = new LinkedHashMap<>();
        // 配置不会被拦截的链接，顺序判断
        // 配置登录请求不需要验证（anon表示某个请求不需要认证）
        filterChainMap.put("/login", "anon");
        filterChainMap.put("/js/**", "anon");
        filterChainMap.put("/css/**", "anon");
        // 配置登出请求，登出后会清除当前用户的内存。
        filterChainMap.put("/logout", "logout");

        // 配置一个admin开头的所有请求需要登录（authc表示需要登录认证）
        // 注意：**--表示任意子孙路径,*--表示任意的一个路径,?--表示任意的一个字符
//        filterChainMap.put("/admin/**", "authc");
//        filterChainMap.put("/user/**", "authc");
        // roles[admin] 表示所有已admin开头的请求需要有admin的角色才可以使用
//        filterChainMap.put("/admin/**","authc,roles[admin]");
        // 配置一个user开头的所有请求需要登录 authc表示需要登录认证
//        filterChainMap.put("/user/**","authc,roles[user]");

        // 配置剩余的所有请求全部都需要登录认证（注意：这个必须写在最后面，不然所有请求皆需要认证）
        filterChainMap.put("/**", "authc"); // 查看Thymeleaf有些标签效果时注释掉，例如：<h1 shiro:guest="true">xxx</h1>

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainMap);

        return shiroFilterFactoryBean;
    }

    /**
     * 开启 Shiro 注解支持 (例如 @RequiresRoles()和 @RequiresPermissions())
     *     shiro 的注解需要借助 Spring 的 AOP 来实现
     *
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        /**
         * 强制使用cglib，防止重复代理和可能引起代理出错的问题
         *  https://zhuanlan.zhihu.com/p/29161098
         */
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    /**
     * 开启AOP的支持
     *
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    /**
     * 配置 Shiro 标签与 Thymeleaf 的集成
     *
     * @return
     */
    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }

}
