# 项目工程介绍
springboot2.0的使用以及各类技术的集成，并且汇聚最终的综合测试小例子。
集成的技术有：
  1、集成web项目，
  2、集成mybatis，
  3、集成redis，
  4、集成dubbo，
  5、集成Thymeleaf，以及对jsp、事务支持、拦截器、serlvet、filter（过滤器）、项目打包方式的练习
  6、其他项目例子，见下方详细项目例子
  
## 1.springboot web项目：springboot-first
项目介绍：web项目，采取jsp渲染前端页面，Interceptor（拦截器）模拟登录拦截日志记录，Filter（过滤器）、servlet使用练习案例

## 2.springboot web项目：springboot-mybatis
项目介绍：springboot+mybatis+redis的web项目，采取mybatis接管数据层、redis缓存数据、springboot集成容器，整合数据层、处理业务。
使用mybatis插件 generator 自定义要求生成代码。

## 3.springboot web项目：springboot-dubbo-*
项目介绍：dubbo案例入门，springboot-dubbo-interface（通用api），springboot-dubbo-provider（dubbo服务提供者）、springboot-dubbo-consumer
（dubbo服务调用者）三个工程组合完成dubbo调用业务入门，技术点：SpringBoot+SpringMvc+dubbo+zookeeper+mybatis。

## 4.springboot java项目：04-springboot-java
项目介绍：纯java项目案例

## 5、springboot web项目：05-springboot-*
项目介绍：05-springboot-jar：springboot web项目的jar打包方式使用案例，05-springboot-war：springboot web项目的war打包方式使用案例

## 6、springboot web项目：06-springboot-thymeleaf
项目介绍：springboot模板引擎 thymeleaf 使用案例

## 7、springboot web项目：07-springboot-*
项目介绍：springboot项目微服务调用案例（springboot+mybatis+dubbo+zookeeper+thymeleaf组合使用案例）

## 8、springboot web项目：08-shiro-demo
项目介绍：简单 springboot 整合 shiro 使用案例。

## 9、springboot web项目：09-security-*
项目介绍：简单的 springboot 整合 security 入门案例：09-security-demo、09-security-jdbc-demo
    入门案例进阶：01-memory-user-details（内存用户信息权限）、02-jdbc-user-details（默认数据表用户权限控制）
        03-user-role（数据库用户角色权限控制案例）、03-user-role-ajax（拥有验证码功能的前后端分离的用户角色权限控制）

## 10、springboot web项目：10-log-demo
项目介绍：简单的 springboot 整合日志 入门案例：01-log4j-demo、02-log4j2-demo、03-logback-demo
分别对应当前主流日志框架，本节主要组合书写了三种常规的书写日志形式。


