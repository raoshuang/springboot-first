package cn.sccdlg.springboot.controller;

import cn.sccdlg.springboot.model.User;
import cn.sccdlg.springboot.service.FirstSpringBootDubboServcie;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FirstSpringBootDubboController {

    @Reference(version = "1.0.0")
    private FirstSpringBootDubboServcie firstSpringBootDubboServcie;

    @RequestMapping("/boot/getUserById")
    @ResponseBody
    public User getUserById(@RequestParam("id") Integer id){
        return firstSpringBootDubboServcie.getUserById(id);
    }

}
