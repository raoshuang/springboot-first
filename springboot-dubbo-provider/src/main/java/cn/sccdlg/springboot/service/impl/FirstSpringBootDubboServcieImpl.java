package cn.sccdlg.springboot.service.impl;

import cn.sccdlg.springboot.mapper.UserMapper;
import cn.sccdlg.springboot.model.User;
import cn.sccdlg.springboot.service.FirstSpringBootDubboServcie;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component //或者是@org.springframework.stereotype.Service 将这个service标记成spring管理的
@Service(version = "1.0.0",timeout = 10000)//dubbo注解配置<dubbo:service interface="" ref=""  version="">
public class FirstSpringBootDubboServcieImpl implements FirstSpringBootDubboServcie {

    @Autowired
    private UserMapper userMapper;

    @Override
    public String sayHi(String name) {
        return "Hi SpringBoot 欢迎您：" + name;
    }

    @Override
    public User getUserById(int id) {
        //查询数据库
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }
}
