package cn.sccdlg.springboot.service;

import cn.sccdlg.springboot.model.User;

/**
 * 第一个springboot集成dubbo的服务接口
 */
public interface FirstSpringBootDubboServcie {
    /**
     * 对访问者的信息回复
     * @param name 访问者
     * @return 返回name的信息回复
     */
    String sayHi(String name);

    /**
     * 根据访问者id，返回访问者的信息
     * @param id 访问者id
     * @return 访问者信息
     */
    User getUserById(int id);
}
