package com.raos.quartz.job;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * CronScheduler
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:50
 */
@Configuration
public class CronScheduler {

    @Bean
    public JobDetail cronJobDetail() {
        return JobBuilder.newJob(CronJob.class)
                .withIdentity("CronJobDetail")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger cronJobTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob(cronJobDetail())
                .withIdentity("cronTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule("9/10 * * * * ? "))
                .build();
    }

}
