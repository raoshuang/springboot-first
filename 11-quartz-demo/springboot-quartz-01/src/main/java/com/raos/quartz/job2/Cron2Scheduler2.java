package com.raos.quartz.job2;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Cron2Scheduler
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:52
 */
@Component
public class Cron2Scheduler2 {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    /**
     * 向容器中添加定时任务
     *
     * @param cron
     * @throws SchedulerException
     */
    public void scheduleJobs(String cron) throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduleJob(scheduler, cron);
    }

    /**
     * 添加一个定时任务
     *
     * @param scheduler
     * @param cron
     * @throws SchedulerException
     */
    private void scheduleJob(Scheduler scheduler, String cron) throws SchedulerException {
        JobDetail jobDetail = JobBuilder.newJob(Cron2Job.class)
                .withIdentity("cron2Job2", "jobGroup2")
                .build();

        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity("myTrigger2", "triggerGroup2")
                .withSchedule(CronScheduleBuilder.cronSchedule(cron))
                .build();
        JobDetail jobDetail1 = scheduler.getJobDetail(jobDetail.getKey());
        if (jobDetail1 == null) {
            scheduler.scheduleJob(jobDetail, cronTrigger);
        } else {
            System.out.println("容器中存在此任务");
        }
    }

}

