package com.raos.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * SampleJob
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:48
 */
public class SampleJob extends QuartzJobBean {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String HELLO_STR = "Hello Spring Boot Quartz! Sample Job doing ...";

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info(HELLO_STR);
        System.out.println(HELLO_STR);
        System.out.println();
    }

}
