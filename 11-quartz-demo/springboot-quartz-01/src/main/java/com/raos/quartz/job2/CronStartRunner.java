package com.raos.quartz.job2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * CronStartRunner
 *  spring容器启动完成后执行器
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:52
 */
@Component
public class CronStartRunner implements CommandLineRunner {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Cron2Scheduler cron2Scheduler;

    @Override
    public void run(String... args) throws Exception {
        cron2Scheduler.scheduleJobs("4/6 * * * * ?");
        log.info(">>定时任务开始执行.....");
    }

}
