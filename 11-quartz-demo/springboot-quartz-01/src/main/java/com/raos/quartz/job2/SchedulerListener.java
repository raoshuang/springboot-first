package com.raos.quartz.job2;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * SchedulerListener
 *  自定义时间点启动定时任务
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:52
 */
@EnableScheduling
@Configuration
@Component
public class SchedulerListener {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Cron2Scheduler2 cron2Scheduler;

    @Scheduled(cron = "30 * * * * ?")
    public void schedule() throws SchedulerException {
        log.info("自定义任务监听器开始执行任务.....");
        cron2Scheduler.scheduleJobs("7/10 * * * * ?");
    }

}
