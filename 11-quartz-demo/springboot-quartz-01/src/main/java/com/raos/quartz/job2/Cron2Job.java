package com.raos.quartz.job2;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Cron 定时任务，项目启动在跑
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:52
 */
public class Cron2Job extends QuartzJobBean {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("项目启动完成, CronJob 执行... ");
    }

}
