package com.raos.quartz.job;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * SampleScheduler
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:48
 */
@Configuration
public class SampleScheduler {

    @Bean
    public JobDetail sampleJobDetail(){
        return JobBuilder.newJob(SampleJob.class)
                .withIdentity("SampleJobDetail")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger sampleJobTrigger(){
        SimpleScheduleBuilder builder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInSeconds(5)
                .repeatForever();
        return TriggerBuilder.newTrigger()
                .forJob(sampleJobDetail())
                // 开始于扫描后启动的3秒
                .startAt(new Date(System.currentTimeMillis() + 3000))
                .withIdentity("sampleTrigger")
                .withSchedule(builder)
                .build();
    }

}
