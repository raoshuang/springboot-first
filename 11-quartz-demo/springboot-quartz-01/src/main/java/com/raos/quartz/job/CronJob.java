package com.raos.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * CronJob
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:50
 */
public class CronJob extends QuartzJobBean {

    private SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("当前时间: " + sf.format(new Date()) + ", CronJob 执行... ");
    }

}
