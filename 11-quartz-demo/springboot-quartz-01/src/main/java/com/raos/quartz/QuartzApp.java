package com.raos.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 集成 Quartz 常规案例
 *
 * @author raos
 * @email 991207823@qq.com
 * @date 2021/10/01 21:48
 */
@SpringBootApplication
public class QuartzApp {

    public static void main(String[] args) {
        SpringApplication.run(QuartzApp.class, args);
    }

}
