package cn.sccdlg.springboot.controller;

import cn.sccdlg.springboot.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class hello {

    @RequestMapping(value = "/index")
    public String index(Model model){
        model.addAttribute("data","Hello Springboot");
        model.addAttribute("msg","第一个集成springboot-thymeleaf集成项目");
        return "index";
    }

    @RequestMapping("/getUser")
    public String getUser(Model model, HttpServletRequest request) {
        User user = new User();
        user.setId("001");
        user.setNick("昵称");
        user.setPhone("18182852797");
        user.setEmail("99120782@qq.com");
        user.setAddress("成都理工工程技术学院");
        model.addAttribute("user",user);
        System.out.println("调用getUser方法，获得user属性为：" + user);

        List<User> userList = new ArrayList<>();
        Map<String,Object> userMap = new HashMap<>();
        User[] users = new User[5];

        for (int i = 0; i < 5; i++) {
            User user1 = new User();
            user1.setId("001" + i);
            user1.setNick("昵称" + i);
            user1.setPhone("18182852797" + "*" + i);
            user1.setEmail("99120782@qq.com" + i);

            userList.add(user1);
            userMap.put(String.valueOf(i),user1);
            users[i] = user1;
        }
        model.addAttribute("userList",userList);
        model.addAttribute("userMap",userMap);
        model.addAttribute("users",users);

        model.addAttribute("username","zhangsan");
        model.addAttribute("sex","2");
        model.addAttribute("isFlag",true);

        request.setAttribute("name","www.baidu.com");
        request.getSession().setAttribute("address","chengdu");

        model.addAttribute("curDate",new Date());

        return "user";
    }
}
