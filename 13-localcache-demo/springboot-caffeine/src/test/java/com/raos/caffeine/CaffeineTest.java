package com.raos.caffeine;

import com.github.benmanes.caffeine.cache.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * 本地缓存 Caffeine 使用入门测试 -- 本案例基于jdk8
 * 官方提醒：For Java 11 or above, use 3.x otherwise use 2.x.
 *
 * @author raos
 * @email 991207823@qq.com
 */
public class CaffeineTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaffeineTest.class);

    /**
     * 手动创建缓存
     */
    @Test
    public void test() {
        Cache<Object, Object> cache = Caffeine.newBuilder()
                // 初始数量
                .initialCapacity(10)
                // 最大条数
                .maximumSize(20)
                // PS：expireAfterWrite和expireAfterAccess同时存在时，以expireAfterWrite为准。
                // 最后一次写操作后经过指定时间过期
                .expireAfterWrite(1, TimeUnit.SECONDS)
                // 最后一次读或写操作后经过指定时间过期
                .expireAfterAccess(1, TimeUnit.SECONDS)
                // 监听缓存被移除
                .removalListener((key, val, removalCause) -> { })
                // 记录命中
                .recordStats()
                .build();

        cache.put("1","张三");
        System.out.println(cache.getIfPresent("1"));
        System.out.println(cache.get("2",o -> "默认值"));
    }

    /**
     * 自动创建缓存
     */
    @Test
    public void test2() {
        LoadingCache<String, Object> loadingCache = Caffeine.newBuilder()
                // 创建缓存或者最近一次更新缓存后经过指定时间间隔，刷新缓存；refreshAfterWrite仅支持LoadingCache
                .refreshAfterWrite(10, TimeUnit.SECONDS)
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .expireAfterAccess(10, TimeUnit.SECONDS)
                .maximumSize(10)
                // 根据key查询数据库里面的值，这里是个lamba表达式
//                .build(key -> System.currentTimeMillis());
                .build(key -> System.nanoTime());

        for (int i = 1; i <= 10; i++) {
            System.out.println(loadingCache.get(i + ""));
        }
    }

    /**
     * 异步获取缓存
     */
    @Test
    public void test3() throws InterruptedException {
        AsyncLoadingCache<String, Object> asyncLoadingCache = Caffeine.newBuilder()
                // 创建缓存或者最近一次更新缓存后经过指定时间间隔刷新缓存；仅支持LoadingCache
                .refreshAfterWrite(10, TimeUnit.SECONDS)
                .expireAfterWrite(10, TimeUnit.SECONDS)
                .expireAfterAccess(10, TimeUnit.SECONDS)
                .maximumSize(10)
                // 根据key查询数据库里面的值
                .buildAsync(key -> {
                    TimeUnit.MILLISECONDS.sleep(10);
                    return System.nanoTime();
                });

        // 异步缓存返回的是CompletableFuture
        for (int i = 1; i <= 10; i++) {
            CompletableFuture<Object> future = asyncLoadingCache.get(i + "");
            future.thenAccept(System.out::println);
        }

        // 等待异步缓存执行完毕
        TimeUnit.SECONDS.sleep(5);
    }

    /**
     * 记录命中率-生产环境不建议开启
     */
    @Test
    public void test4() throws IOException {
        LoadingCache<String, Object> cache = Caffeine.newBuilder()
                // 创建缓存或者最近一次更新缓存后经过指定时间间隔，刷新缓存；refreshAfterWrite仅支持LoadingCache
                .refreshAfterWrite(1, TimeUnit.SECONDS)
                .expireAfterWrite(1, TimeUnit.SECONDS)
                .expireAfterAccess(1, TimeUnit.SECONDS)
                .maximumSize(10)
                // 开启记录缓存命中率等信息
                .recordStats()
                // 根据key查询数据库里面的值
                .build(key -> {
                    TimeUnit.MILLISECONDS.sleep(20);
                    return System.currentTimeMillis();
                });

        for (int i = 1; i <= 10; i++) {
            System.out.println(cache.get(i + ""));
        }

        cache.put("1", "小明");
        System.out.println(cache.get("1"));

        /*
         * hitCount:    命中的次数
         * missCount:   未命中次数
         * requestCount:请求次数
         * hitRate:命中率
         * missRate:丢失率
         * loadSuccessCount:成功加载新值的次数
         * loadExceptionCount:失败加载新值的次数
         * totalLoadCount:总条数
         * loadExceptionRate:失败加载新值的比率
         * totalLoadTime:全部加载时间
         * evictionCount:丢失的条数
         */
        System.out.println(cache.stats());
    }

    // 淘汰策略应用
    /**
     * 缓存大小淘汰
     */
    @Test
    public void maximumSizeTest() throws InterruptedException {
        Cache<Integer, Integer> cache = Caffeine.newBuilder()
                // 超过10个后会使用W-TinyLFU算法进行淘汰
                .maximumSize(10)
                .evictionListener((key, val, removalCause) -> {
                    LOGGER.info("淘汰缓存：key:{} val:{}", key, val);
                })
                .build();

        for (int i = 1; i <= 20; i++) {
            cache.put(i, i);
        }

        Thread.sleep(500); // 缓存淘汰是异步的，给出反应时间

        // 打印还没被淘汰的缓存
        System.out.println(cache.asMap());
    }

    /**
     * 权重淘汰
     */
    @Test
    public void maximumWeightTest() throws InterruptedException {
        Cache<Integer, Integer> cache = Caffeine.newBuilder()
                // 限制总权重，若所有缓存的权重加起来>总权重就会淘汰权重小的缓存
                .maximumWeight(100)
                .weigher((Weigher<Integer, Integer>) (key, value) -> key)
                .evictionListener((key, val, removalCause) -> {
                    LOGGER.info("淘汰缓存：key:{} val:{}", key, val);
                })
                .build();

        //总权重其实是=所有缓存的权重加起来
        int maximumWeight = 0;
        for (int i = 1; i <= 20; i++) {
            cache.put(i, i);
            maximumWeight += i;
        }
        System.out.println("总权重=" + maximumWeight);
        Thread.sleep(500);//缓存淘汰是异步的

        // 打印还没被淘汰的缓存
        System.out.println(cache.asMap());
    }

    /**
     * 访问后到期（每次访问都会重置时间，也就是说如果一直被访问就不会被淘汰）
     */
    @Test
    public void expireAfterAccessTest() throws InterruptedException {
        Cache<Integer, Integer> cache = Caffeine.newBuilder()
                .expireAfterAccess(1, TimeUnit.SECONDS)
                // 可以指定调度程序来及时删除过期缓存项，而不是等待Caffeine触发定期维护
                // 若不设置scheduler，则缓存会在下一次调用get的时候才会被动删除
                .scheduler(Scheduler.systemScheduler())
                .evictionListener((key, val, removalCause) -> {
                    LOGGER.info("淘汰缓存：key:{} val:{}", key, val);
                })
                .build();
        cache.put(1, 2);
        System.out.println(cache.getIfPresent(1));
        Thread.sleep(2000);
        System.out.println(cache.getIfPresent(1)); //null
    }

    /**
     * 写入后到期
     */
    @Test
    public void expireAfterWriteTest() throws InterruptedException {
        Cache<Integer, Integer> cache = Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.SECONDS)
                // 可以指定调度程序来及时删除过期缓存项，而不是等待Caffeine触发定期维护
                // 若不设置scheduler，则缓存会在下一次调用get的时候才会被动删除
                .scheduler(Scheduler.systemScheduler())
                .evictionListener((key, val, removalCause) -> {
                    LOGGER.info("淘汰缓存：key:{} val:{}", key, val);
                })
                .build();
        cache.put(1, 2);
        System.out.println(cache.getIfPresent(1));
        Thread.sleep(3000);
        System.out.println(cache.getIfPresent(1)); //null
    }

    private static int NUM = 0;

    /**
     * 自动刷新缓存
     */
    @Test
    public void refreshAfterWriteTest() throws InterruptedException {
        LoadingCache<Integer, Integer> cache = Caffeine.newBuilder()
                .refreshAfterWrite(1000, TimeUnit.MILLISECONDS)
                // 模拟获取数据，每次获取就自增1
                .build(integer -> ++NUM);

        // 获取ID=1的值，由于缓存里还没有，所以会自动放入缓存
        LOGGER.info("{}", cache.get(1)); // 1

        // 延迟2秒后，理论上自动刷新缓存后取到的值是2（实际上新版也是）
        // 但其实不是，值还是1，因为refreshAfterWrite并不是设置了n秒后重新获取就会自动刷新
        // 而是x秒后&&第二次调用getIfPresent的时候才会被动刷新

        Thread.sleep(2000);
        LOGGER.info("{}", cache.getIfPresent(1)); // 2

        Thread.sleep(1000);
        // 此时才会刷新缓存，而第一次拿到的还是旧值
        LOGGER.info("{}", cache.getIfPresent(1)); // 2
    }

    /*
     * 最佳实际总结：
     * - 实践1
     * 配置：设置 maxSize、refreshAfterWrite，不设置 expireAfterWrite/expireAfterAccess
     * 优缺点：因为设置expireAfterWrite当缓存过期时会同步加锁获取缓存，所以设置expireAfterWrite时性能较好，
     * 但是某些时候会取旧数据,适合允许取到旧数据的场景
     *
     * - 实践2 配置：设置 maxSize、expireAfterWrite/expireAfterAccess，不设置 refreshAfterWrite
     * 优缺点：与上面相反，数据一致性好，不会获取到旧数据，但是性能没那么好（对比起来），适合获取数据时不耗时的场景
     */

}
