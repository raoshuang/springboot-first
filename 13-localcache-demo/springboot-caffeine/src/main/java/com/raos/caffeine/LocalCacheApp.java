package com.raos.caffeine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 集成 caffeine 常规案例
 *
 * @author raos
 * @email 991207823@qq.com
 */
@SpringBootApplication
public class LocalCacheApp {

    public static void main(String[] args) {
        SpringApplication.run(LocalCacheApp.class, args);
    }

}
