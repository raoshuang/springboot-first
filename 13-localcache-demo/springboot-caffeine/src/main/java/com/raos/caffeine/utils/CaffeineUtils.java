package com.raos.caffeine.utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.raos.caffeine.config.CaffeineConfig;

/**
 * Caffeine缓存工具类
 *
 * @author raos
 * @email 991207823@qq.com
 */
@SuppressWarnings("all")
public class CaffeineUtils {

    private static Cache<String, Object> caffeineCache;

    static {
        caffeineCache = (Cache<String, Object>) SpringUtils.getBean(CaffeineConfig.ONE_CACHE_NAME);
    }

    public static void put(String key, Object value) {
        caffeineCache.put(key, value);
    }

    public static Object get(String key) {
        return caffeineCache.asMap().get(key);
    }

}
